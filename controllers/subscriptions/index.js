const { CtrlWrapper } = require('../../helpers');
const addSubscription = require('./addSubscription');

module.exports = {
  addSubscription: CtrlWrapper(addSubscription),
};
