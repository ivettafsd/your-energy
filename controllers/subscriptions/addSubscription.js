const { Subscription } = require('../../models/subscription');
const { HttpError } = require('../../helpers');

const addSubscription = async (req, res) => {
  const { email } = req.body;

  const foundSubscription = await Subscription.findOne({ email });

  if (foundSubscription) {
    throw HttpError(409, 'Subscription already exists');
  }

  await Subscription.create({ email });

  const message = `We're excited to have you on board! 🎉 Thank you for subscribing to new exercises on Your Energy. You've just taken a significant step towards improving your fitness and well-being.`;
  res.status(201).json({ message });
};

module.exports = addSubscription;
