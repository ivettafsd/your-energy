const { CtrlWrapper } = require('../../helpers');
const getFilterList = require('./getFilterList');

module.exports = {
  getFilterList: CtrlWrapper(getFilterList),
};
