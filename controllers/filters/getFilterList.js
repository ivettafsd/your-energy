const { Filter } = require('../../models/filter');

const getFilterList = async (req, res) => {
  const { filter = '', page = 1, limit = 12 } = req.query;

  const query = {};
  filter && (query.filter = filter);
  const skip = (page - 1) * limit;

  const foundFilters = await Filter.aggregate([
    { $match: query },
    {
      $facet: {
        totalCount: [{ $count: 'count' }],
        results: [{ $skip: Number(skip) }, { $limit: Number(limit) }, { $unset: ['_id'] }],
      },
    },
    {
      $project: {
        totalCount: { $arrayElemAt: ['$totalCount.count', 0] },
        page: page,
        perPage: limit,
        results: 1,
      },
    },
  ]);
  res.json({
    page,
    perPage: limit,
    totalPages: Math.ceil(foundFilters[0].totalCount / limit),
    results: foundFilters[0].results,
  });
};

module.exports = getFilterList;
