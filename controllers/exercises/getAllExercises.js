const { Exercise } = require('../../models/exercise');
const { HttpError } = require('../../helpers');

const getAllExercises = async (req, res) => {
  const { bodypart = '', muscles = '', equipment = '', keyword = '', page = 1, limit = 10 } = req.query;

  const query = {};
  bodypart && (query.bodyPart = bodypart);
  equipment && (query.equipment = equipment);
  muscles && (query.target = muscles);
  keyword && (query.name = { $regex: keyword, $options: 'i' });

  if (keyword && !bodypart && !equipment && !muscles) {
    throw HttpError(409, 'Filter is required');
  }

  const skip = (page - 1) * limit;

  const foundExercises = await Exercise.aggregate([
    { $match: query },
    {
      $facet: {
        totalCount: [{ $count: 'count' }],
        results: [
          { $skip: Number(skip) },
          { $limit: Number(limit) },
          { $unset: ['createdAt', 'updatedAt', 'whoRated'] }, // Hide the original whoRated array
        ],
      },
    },
    {
      $project: {
        totalCount: { $arrayElemAt: ['$totalCount.count', 0] },
        page: page,
        perPage: limit,
        results: 1,
      },
    },
  ]);

  res.json({
    page: Number(page),
    perPage: Number(limit),
    totalPages: Math.ceil(foundExercises[0].totalCount / limit),
    results: foundExercises[0].results,
  });
};

module.exports = getAllExercises;
