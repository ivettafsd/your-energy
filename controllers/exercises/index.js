const { CtrlWrapper } = require('../../helpers');
const getAllExercises = require('./getAllExercises');
const getExerciseById = require('./getExerciseById');
const addRatingById = require('./addRatingById');

module.exports = {
  getAllExercises: CtrlWrapper(getAllExercises),
  getExerciseById: CtrlWrapper(getExerciseById),
  addRatingById: CtrlWrapper(addRatingById),
};
