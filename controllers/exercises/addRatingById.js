const { Exercise } = require('../../models/exercise');
const { HttpError } = require('../../helpers');
const { Types } = require('mongoose');
const ObjectId = Types.ObjectId;

const addRatingById = async (req, res) => {
  const { id } = req.params;
  const newRate = req.body;

  const foundExercise = await Exercise.findById(id);
  if (!foundExercise) {
    throw HttpError(404, 'Such exercise not found');
  }

  const isOldUser = foundExercise.whoRated.find((item) => item.email === newRate.email);
  if (isOldUser) {
    throw HttpError(409, 'Such email already exists');
  }

  const exerciseWithNewRating = {
    rating: +((foundExercise.rating * foundExercise.whoRated.length + newRate.rate) / (foundExercise.whoRated.length + 1)).toFixed(2),
    whoRated: [...foundExercise.whoRated, { email: newRate.email, review: newRate.review }],
  };
  await Exercise.findByIdAndUpdate(id, exerciseWithNewRating, { new: true });

  const exercise = await Exercise.aggregate([
    {
      $match: {
        _id: ObjectId.createFromHexString(id),
      },
    },
    {
      $unset: ['createdAt', 'updatedAt', 'whoRated'],
    },
  ]);
  res.json(exercise[0]);
};

module.exports = addRatingById;
