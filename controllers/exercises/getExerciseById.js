const { Exercise } = require('../../models/exercise');
const { Types } = require('mongoose');
const { HttpError } = require('../../helpers');
const ObjectId = Types.ObjectId;

const getExerciseById = async (req, res) => {
  const { id } = req.params;

  const updatedExercise = await Exercise.findByIdAndUpdate(
    ObjectId.createFromHexString(id),
    {
      $inc: { popularity: 1 },
    },
    { new: true, select: '-createdAt -updatedAt -whoRated' },
  );
  if (!updatedExercise) {
    throw HttpError(404, 'Not found');
  }

  res.json(updatedExercise);
};

module.exports = getExerciseById;
