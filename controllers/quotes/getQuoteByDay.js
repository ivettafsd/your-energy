const { Quote } = require('../../models/quote');

const getQuoteByDay = async (req, res) => {
  const currentDate = new Date();
  const currentDayNumber = currentDate.getDate();

  const {author, quote} = await Quote.findOne({ day: currentDayNumber });
  res.json({ author, quote });
};

module.exports = getQuoteByDay;
