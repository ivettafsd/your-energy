const { CtrlWrapper } = require('../../helpers');
const getQuoteByDay = require('./getQuoteByDay');

module.exports = {
  getQuoteByDay: CtrlWrapper(getQuoteByDay),
};
