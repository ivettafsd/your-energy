const express = require('express');
const ctrl = require('../../controllers/exercises');
const { validateBody, isValidId } = require('../../middlewares');
const { schemas } = require('../../models/exercise');

const router = express.Router();

router.get('/', ctrl.getAllExercises);
router.patch('/:id/rating', isValidId, validateBody(schemas.addRateSchema), ctrl.addRatingById);
router.get('/:id', isValidId, ctrl.getExerciseById);

module.exports = router;
