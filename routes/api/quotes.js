const express = require('express');
const ctrl = require('../../controllers/quotes');

const router = express.Router();

router.get('/', ctrl.getQuoteByDay);

module.exports = router;
