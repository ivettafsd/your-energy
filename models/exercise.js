const { handleMangooseError } = require('../helpers');
const { Schema, model } = require('mongoose');
const Joi = require('joi');

const emailRegexp = /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/;

const exerciseSchema = new Schema(
  {
    bodyPart: {
      type: String,
      required: true,
    },
    equipment: {
      type: String,
      required: true,
    },
    gifUrl: {
      type: String,
      required: true,
    },
    name: {
      type: String,
      required: true,
    },
    target: {
      type: String,
      required: true,
    },
    description: {
      type: String,
      required: true,
    },
    rating: {
      type: Number,
      required: true,
      default: 3,
    },
    whoRated: {
      type: [
        {
          email: {
            type: String,
            match: emailRegexp,
          },
          review: {
            type: String,
            max: 250,
          },
        },
      ],
      default: [],
      required: true,
    },
    popularity: {
      type: Number,
      required: true,
      default: 0
    },
    time: {
      type: Number,
      required: false,
    },
    burnedCalories: {
      type: Number,
      required: false,
    },
  },
  { versionKey: false, timestamps: true },
);

exerciseSchema.post('save', handleMangooseError);

const addRateSchema = Joi.object({
  rate: Joi.number().strict().required().min(1).max(5).messages({
    'number.base': 'The rate must be a number.',
    'number.min': 'The rate must be at least 1.',
    'number.max': 'The rate cannot exceed 5.',
    'any.required': 'The rate field is required.',
  }),
  email: Joi.string().pattern(emailRegexp).required().messages({
    'string.base': 'The email must be a string.',
    'string.pattern.base': 'The email must be in format test@gmail.com.',
    'any.required': 'The email field is required.',
  }),
  review: Joi.string().max(250).messages({
    'string.max': 'The review length cannot more 250.'
  }),
});

const Exercise = model('exercise', exerciseSchema);

const schemas = { addRateSchema };

module.exports = { Exercise, schemas };
