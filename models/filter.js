const { handleMangooseError } = require('../helpers');
const { Schema, model } = require('mongoose');

const filterSchema = new Schema(
  {
    _id: {
      _id: false,
      type: String,
      required: true,
    },
    filter: {
      type: String,
      enum: ['Body parts', 'Muscles', 'Equipment'],
      required: true,
    },
    name: {
      type: String,
      required: true,
    },
    imgUrl: {
      type: String,
      required: true,
    },
  },
  { versionKey: false, timestamps: true },
);

filterSchema.post('save', handleMangooseError);

const Filter = model('filter', filterSchema);

module.exports = { Filter };
