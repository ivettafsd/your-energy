const { handleMangooseError } = require('../helpers');
const { Schema, model } = require('mongoose');

const quoteSchema = new Schema(
  {
    _id: {
      _id: false,
      type: String,
      required: true,
    },
    day: {
      type: Number,
      required: true,
    },
    author: {
      type: String,
      required: true,
    },
    quote: {
      type: String,
      required: true,
    },
  },
  { versionKey: false, timestamps: true },
);

quoteSchema.post('save', handleMangooseError);

const Quote = model('quote', quoteSchema);

module.exports = { Quote };
