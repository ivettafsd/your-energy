const { handleMangooseError } = require('../helpers');
const { Schema, model } = require('mongoose');
const Joi = require('joi');

const emailRegexp = /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/;

const subscriptionSchema = new Schema(
  {
    email: {
      type: String,
      match: emailRegexp,
      unique: true,
      required: true,
    },
  },
  { versionKey: false, timestamps: true },
);

subscriptionSchema.post('save', handleMangooseError);

const addSubscriptionSchema = Joi.object({
  email: Joi.string().pattern(emailRegexp).required().messages({
    'string.base': 'The email must be a string.',
    'string.pattern.base': 'The email must be in format test@gmail.com.',
    'any.required': 'The email field is required.',
  }),
});

const Subscription = model('subscription', subscriptionSchema);

const schemas = { addSubscriptionSchema };

module.exports = { Subscription, schemas };
